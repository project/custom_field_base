<?php

namespace Drupal\custom_field_base_test_fields\Plugin\Field\FieldType;

use Drupal\custom_field_base\Plugin\Field\FieldType\CustomFieldBase;

/**
 * Custom field type used for tests.
 *
 * @FieldType(
 *   id = "custom_field_base_test_fields_single_string_max_length",
 *   label = @Translation("custom_field_base_test_fields: single_string_max_length"),
 *   description = @Translation("") * )
 */
class TestStringMaxLength extends CustomFieldBase {

  /**
   * {@inheritdoc}
   */
  public static function getAllProperties() {
    $props = [];
    $props['mykey'] = [
      "title" => "String title",
      "type" => "string",
      "required" => FALSE,
      "max_length" => 20,

    ];
    return $props;

  }

}
