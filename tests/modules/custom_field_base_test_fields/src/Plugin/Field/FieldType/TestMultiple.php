<?php

namespace Drupal\custom_field_base_test_fields\Plugin\Field\FieldType;

use Drupal\custom_field_base\Plugin\Field\FieldType\CustomFieldBase;

/**
 * Custom field type used for tests.
 *
 * @FieldType(
 *   id = "custom_field_base_test_fields_multiple",
 *   label = @Translation("custom_field_base_test_fields: single_map"),
 *   description = @Translation("") * )
 */
class TestMultiple extends CustomFieldBase {

  /**
   * {@inheritdoc}
   */
  public static function getAllProperties() {
    $props = [];
    $props['one'] = [
      "type" => "integer",
    ];
    $props['two'] = [
      "type" => "float",
    ];
    $props['three'] = [
      "type" => "string",
    ];
    $props['value'] = [
      "type" => "string",
    ];

    return $props;

  }

}
