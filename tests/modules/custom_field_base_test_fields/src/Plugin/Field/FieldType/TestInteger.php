<?php

namespace Drupal\custom_field_base_test_fields\Plugin\Field\FieldType;

use Drupal\custom_field_base\Plugin\Field\FieldType\CustomFieldBase;

/**
 * Custom field type used for tests.
 *
 * @FieldType(
 *   id = "custom_field_base_test_fields_single_integer",
 *   label = @Translation("custom_field_base_test_fields: single_string"),
 *   description = @Translation("") * )
 */
class TestInteger extends CustomFieldBase {

  /**
   * {@inheritdoc}
   */
  public static function getAllProperties() {
    $props = [];
    $props['mykey'] = [
      "title" => "title",
      "type" => "integer",
      "required" => FALSE,
    ];
    return $props;

  }

}
