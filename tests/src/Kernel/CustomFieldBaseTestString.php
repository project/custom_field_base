<?php

namespace Drupal\Tests\custom_field_base\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests the raw string formatter.
 *
 * @group custom_field_base
 */
class CustomFieldBaseTestString extends CustomFieldBaseTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->fieldType = "custom_field_base_test_fields_single_string";
    parent::setUp();
  }

  /**
   * Test reading and writing values into the field.
   */
  public function testReadWrite() {
    $value = $this->randomString();
    $value .= "\n\n<strong>" . $this->randomString() . '</strong>';
    $value .= "\n\n" . $this->randomString();

    $entity = EntityTest::create([]);
    $get_value = $entity->{$this->fieldName}[] = [
      "mykey" => $value,
    ];
    // $entity->{$this->fieldName}->mykey = $value;
    $get_value = $entity->{$this->fieldName}->get(0)->getValue()["mykey"];

    $this->assertEqual($get_value, $value);

    // -----
    $entity = EntityTest::create([]);
    $entity->{$this->fieldName}->mykey = $value;

    $get_value = $entity->{$this->fieldName}->get(0)->getValue()["mykey"];
  }

}
