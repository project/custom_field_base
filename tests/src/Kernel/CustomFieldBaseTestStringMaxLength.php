<?php

namespace Drupal\Tests\custom_field_base\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests the raw string formatter.
 *
 * @group custom_field_base
 */
class CustomFieldBaseTestStringMaxLength extends CustomFieldBaseTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->fieldType = "custom_field_base_test_fields_single_string_max_length";
    parent::setUp();
  }

  /**
   * Test reading and writing values into the field.
   */
  public function testReadWrite() {

    $value = $this->randomString(40);

    $cut_value = substr($value, 0, 20);

    $entity = EntityTest::create([]);

    try {
      $get_value = $entity->{$this->fieldName}[] = [
        "mykey" => $value,
      ];
      // $entity->save();
      // $etm = \Drupal::entityTypeManager();
      // $entity  = $etm->getStorage($this->entityType)->load($entity->id());
      // $entity->{$this->fieldName}->mykey = $value;
      $get_value = $entity->{$this->fieldName}->get(0)->getValue()["mykey"];

      $this->assertNotEqual($get_value, $value);
      $this->assertEqual($get_value, $cut_value);
    } finally {
      if ($entity->id()) {
        $entity->delete();
      }
    }

  }

}
