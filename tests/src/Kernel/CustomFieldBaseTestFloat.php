<?php

namespace Drupal\Tests\custom_field_base\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests the raw string formatter.
 *
 * @group custom_field_base
 */
class CustomFieldBaseTestFloat extends CustomFieldBaseTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->fieldType = "custom_field_base_test_fields_single_float";
    parent::setUp();

  }

  /**
   * Generates field values.
   *
   * @return array
   *   array of values
   */
  public function valuesProvider() {
    return [
        ["value" => 0.334],
          ["value" => 0.0],
          ["value" => 1234.66666],
          ["value" => 4565.66666],
    ];

  }

  /**
   * Test reading and writing values into the field.
   *
   * @dataProvider valuesProvider
   */
  public function testReadWrite($value) {
    $entity = EntityTest::create([]);
    $get_value = $entity->{$this->fieldName}[] = [
      "mykey" => $value,
    ];
    // $entity->{$this->fieldName}->mykey = $value;
    $get_value = $entity->{$this->fieldName}->get(0)->getValue()["mykey"];

    $this->assertEqual($get_value, $value);
    // -----
    $entity = EntityTest::create([]);
    $entity->{$this->fieldName}->mykey = $value;

    $get_value = $entity->{$this->fieldName}->get(0)->getValue()["mykey"];
    $this->assertEqual($get_value, $value);
  }

}
