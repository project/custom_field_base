<?php

namespace Drupal\Tests\custom_field_base\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests the raw string formatter.
 *
 * @group custom_field_base
 */
class CustomFieldBaseTestIntegerUnsigned extends CustomFieldBaseTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->fieldType = "custom_field_base_test_fields_single_integer_unsigned";
    parent::setUp();

  }

  /**
   * Generates field values.
   *
   * @return array
   *   array of values
   */
  public function valuesProvider() {
    return [
          ["value" => -1],
          ["value" => -22],
    ];

  }

  /**
   * Test reading and writing values into the field.
   *
   * @dataProvider valuesProvider
   */
  public function testReadWrite($value) {

    $entity = EntityTest::create([]);
    try {
      $get_value = $entity->{$this->fieldName}[] = [
        "mykey" => $value,
      ];
      $this->fail("expected exception");
    }
    catch (\Exception $e) {
      $this->pass("expected exception ok");
    }
  }

}
