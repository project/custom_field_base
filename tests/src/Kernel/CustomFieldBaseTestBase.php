<?php

namespace Drupal\Tests\custom_field_base\Kernel;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the raw string formatter.
 *
 * @group custom_field_base
 */
abstract class CustomFieldBaseTestBase extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'field',
    'text',
    'entity_test',
    'system',
    'filter',
    'user',
    'custom_field_base_test_fields',
  ];

  /**
   * Entity type used for the tests.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Bundle type used for the tests.
   *
   * @var string
   */
  protected $bundle;

  /**
   * Field name type used for the tests.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * Field type type used for the tests.
   *
   * @var string
   */
  protected $fieldType;

  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Configure the theme system.
    $this->installConfig(['system', 'field']);
    \Drupal::service('router.builder')->rebuild();
    $this->installEntitySchema('entity_test');

    $this->entityType = 'entity_test';
    $this->bundle = $this->entityType;
    $this->fieldName = mb_strtolower($this->randomMachineName());

    if ($this->fieldType) {
      $this->setUpFieldType($this->fieldType);
    }
  }

  /**
   * Set up the field type for testing.
   *
   * @param string $field_type
   *   Type of the field to set up.
   */
  protected function setUpFieldType($field_type) {
    $field_storage = FieldStorageConfig::create([
      'field_name' => $this->fieldName,
      'entity_type' => $this->entityType,
      'type' => $field_type,
    ]);
    $field_storage->save();

    $instance = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $this->bundle,
      'label' => $this->randomMachineName(),
    ]);
    $instance->save();
    /*
    $this->display = \Drupal::service('entity_display.repository')
    ->getViewDisplay($this->entityType, $this->bundle)
    ->setComponent($this->fieldName, [
    'type' => 'string',
    'settings' => [],
    ]);
    $this->display->save();
     */
  }

  /**
   * Renders fields of a given entity with a given display.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity object with attached fields to render.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The display to render the fields in.
   *
   * @return string
   *   The rendered entity fields.
   */
  protected function renderEntityFields(FieldableEntityInterface $entity, EntityViewDisplayInterface $display) {
    $content = $display->build($entity);
    $content = $this->render($content);
    return $content;
  }

}
