<?php

namespace Drupal\Tests\custom_field_base\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests the raw string formatter.
 *
 * @group custom_field_base
 */
class CustomFieldBaseTestMultiple extends CustomFieldBaseTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->fieldType = "custom_field_base_test_fields_multiple";
    parent::setUp();

  }

  /**
   * Generates field values.
   *
   * @return array
   *   array of values
   */
  public function valuesProvider() {
    return [
        [
          "value" => [
            "one" => 22,
            "two" => 22.22,
            "three" => "value",
            "value" => "value",
          ],
        ],
    ];

  }

  /**
   * Test reading and writing values into the field.
   *
   * @dataProvider valuesProvider
   */
  public function testReadWrite($value) {
    $entity = EntityTest::create([]);
    $get_value = $entity->{$this->fieldName}[] = $value;
    // $entity->{$this->fieldName}->mykey = $value;
    $get_value = $entity->{$this->fieldName}->get(0)->getValue();

    $this->assert(is_array($get_value));

    foreach ($value as $kk => $vv) {
      $this->assert(array_key_exists($kk, $get_value));
      $this->assertEqual($get_value[$kk], $value[$kk]);
    }

    // -----
    $entity = EntityTest::create([]);
    $entity->{$this->fieldName} = $value;

    $get_value = $entity->{$this->fieldName}->get(0)->getValue();
    $this->assert(is_array($get_value));

    foreach ($value as $kk => $vv) {
      $this->assert(array_key_exists($kk, $get_value));
      $this->assertEqual($get_value[$kk], $value[$kk]);
    }
    // ----
    $entity = EntityTest::create([]);
    $get_value = $entity->{$this->fieldName}[] = [];
    // $entity->{$this->fieldName}->mykey = $value;
    foreach ($value as $kk => $vv) {
      $entity->{$this->fieldName}->{$kk} = $vv;
    }

    $get_value = $entity->{$this->fieldName}->get(0)->getValue();

    $this->assert(is_array($get_value));

    foreach ($value as $kk => $vv) {
      $this->assert(array_key_exists($kk, $get_value));
      $this->assertEqual($get_value[$kk], $value[$kk]);
    }

    // --------
    $entity = EntityTest::create([]);
    foreach ($value as $kk => $vv) {
      $entity->{$this->fieldName}->{$kk} = $vv;
    }

    $get_value = $entity->{$this->fieldName}->get(0)->getValue();

    $this->assert(is_array($get_value));

    foreach ($value as $kk => $vv) {
      $this->assert(array_key_exists($kk, $get_value));
      $this->assertEqual($get_value[$kk], $value[$kk]);
    }

  }

}
