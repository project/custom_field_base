<?php

namespace Drupal\custom_field_example\Plugin\Field\FieldWidget;

use Drupal\custom_field_example\Plugin\Field\FieldType\CustomFieldExample;
use Drupal\custom_field_base\Plugin\Field\FieldWidget\CustomFieldBaseWidget;

/**
 * Example of custom field widget for field type custom_field_example.
 *
 * @FieldWidget(
 *   id = "custom_field_example",
 *   module = "custom_field_example",
 *   label = @Translation("example of custom_field_base field widget"),
 *   field_types = {
 *     "custom_field_example"
 *   }
 * )
 */
class CustomFieldExampleWidget extends CustomFieldBaseWidget {

  /**
   * {@inheritdoc}
   */
  public static function getAllProperties() {
    return CustomFieldExample::getAllProperties();
  }

}
