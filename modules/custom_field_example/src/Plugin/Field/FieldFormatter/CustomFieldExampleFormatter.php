<?php

namespace Drupal\custom_field_example\Plugin\Field\FieldFormatter;

use Drupal\custom_field_example\Plugin\Field\FieldType\CustomFieldExample;
use Drupal\custom_field_base\Plugin\Field\FieldFormatter\CustomFieldBaseFormatter;

/**
 * Example of custom field formatter for field type custom_field_example.
 *
 * @FieldFormatter(
 *   id = "custom_field_example",
 *   label = @Translation("example of custom_field_base field formatter"),
 *   field_types = {
 *     "custom_field_example"
 *   }
 * )
 */
class CustomFieldExampleFormatter extends CustomFieldBaseFormatter {

  /**
   * {@inheritdoc}
   */
  public static function getAllProperties() {
    return CustomFieldExample::getAllProperties();
  }

}
