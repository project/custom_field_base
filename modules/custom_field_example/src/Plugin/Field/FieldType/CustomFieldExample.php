<?php

namespace Drupal\custom_field_example\Plugin\Field\FieldType;

use Drupal\custom_field_base\Plugin\Field\FieldType\CustomFieldBase;

/**
 * Example of custom field type.
 *
 * @FieldType(
 *   id = "custom_field_example",
 *   label = @Translation("example of custom_field_base field type"),
 *   default_widget = "custom_field_example",
 *   default_formatter= "custom_field_example"
 * )
 */
class CustomFieldExample extends CustomFieldBase {

  /**
   * {@inheritdoc}
   */
  public static function getAllProperties() {
    $props = [];
    $props['one'] = [
      "type" => "integer",
      "title" => "Property title",
      "required" => FALSE,
    // Valid values: 'tiny', 'small', 'medium', 'normal' and 'big'.
      "size" => "normal",
      "unsigned" => FALSE,
    ];
    $props['two'] = [
      "type" => "float",
    ];
    $props['three'] = [
      "type" => "string",
      "is_ascii" => FALSE,
      "case_sensitive" => FALSE,
      "max_length" => 255,
    ];
    return $props;
  }

}
