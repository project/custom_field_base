CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Usage
 * Roadmap
 * Maintainers


INTRODUCTION
------------

The Custom Field Base module provides base classes to facilitate the declaration
of new field types (using PHP code). The idea behind this module is to provide
a simple way to declare new field types with multiple properties. Through base
classes, it also facilitates the declaration of compatible field widgets and
field formatters.

The Custom Field Base module is a module without User Interface, it is for PHP
developers only.


REQUIREMENTS
------------

This module has no requirements.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


USAGE
-----

This module provides an example module "custom_field_example", which
demonstrates how to the use this module.


ROADMAP
-------

Before its first stable release, this module will be updated in order to achieve
 the following goals:
  * support more basic properties
  * better support for views
  * more flexibility for widget and formatter definition


MAINTAINERS
-----------

Current maintainer(s):

 * Mikael Meulle - https://www.drupal.org/u/just_like_good_vibes

