<?php

namespace Drupal\custom_field_base\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Component\Utility\UrlHelper;
use \Drupal\link\LinkItemInterface;

/**
 * A base class to faciliate declaration of new field types.
 */
abstract class CustomFieldBase extends FieldItemBase
{

    /**
     * Returns configuration schema data for field storage settings.
     *
     * This is typically to be used in an implementation
     * of hook_config_schema_info_alter.
     *
     * @return mixed
     *   configuration schema value
     */
    public static function generateConfigSchemaFieldStorageSettings()
    {
        $schema = [
        "type" => "mapping",
        "label" => "Storage Settings",
        "mapping" => [],
        ];
        $props = static::getAllProperties();

        foreach ($props as $prop_key => $prop_data) {
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
            $schema["mapping"][$prop_key] = [
            "type" => "mapping",
            "label" => "Storage Settings for propery " . $prop_key,
            "mapping" => [],
            ];
            switch ($prop_data_type) {
            case "float":
                break;

            case "integer":
                $schema["mapping"][$prop_key]["mapping"]["unsigned"] = [
                "type" => "boolean",
                "label" => 'Unsigned',
                ];
                $schema["mapping"][$prop_key]["mapping"]["size"] = [
                "type" => "string",
                "label" => 'Database storage size',
                ];
                break;

            case 'string':
                break;

            case 'boolean':
                break;

            }
        }
        return $schema;
    }

    /**
     * Returns configuration schema data for field settings.
     *
     * This is typically to be used in an implementation
     * of hook_config_schema_info_alter.
     *
     * @return mixed
     *   configuration schema value
     */
    public static function generateConfigSchemaFieldSettings()
    {
        $props = static::getAllProperties();
        foreach ($props as $prop_key => $prop_data) {
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
            switch ($prop_data_type) {
            case "float":
            case "integer":
                $ret[$prop_key] = [
                'min' => '',
                'max' => '',
                'prefix' => '',
                'suffix' => '',
                ];
                break;

            case "boolean":
                $ret[$prop_key] = [];
                break;

            case 'string':
                $ret[$prop_key] = [
                'max_length' => 255,
                'is_ascii' => false,
                ];
                break;
            case "uri":
                  $ret[$prop_key] = [
                  ];
                break;

            }
        }
    }

    /**
     * Returns configuration schema data for field value.
     *
     * This is typically to be used in an implementation
     * of hook_config_schema_info_alter.
     *
     * @return mixed
     *   configuration schema value
     */
    public static function generateConfigSchemaFieldValue()
    {
    }

    /**
     * Returns the default storage settings.
     *
     * @return mixed
     *   default settings
     */
    protected static function defaultStorageSettingsCustomFieldBase()
    {
        $ret = [];
        $props = static::getAllProperties();
        foreach ($props as $prop_key => $prop_data) {
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
            switch ($prop_data_type) {
            case "float":
            case "integer":
                $ret[$prop_key] = [
                'min' => '',
                'max' => '',
                'prefix' => '',
                'suffix' => '',
                ];
                break;

            case "boolean":
                $ret[$prop_key] = [];
                break;

            case 'string':
                $ret[$prop_key] = [
                'max_length' => 255,
                'is_ascii' => false,
                ];
                break;
            case "uri":
                $ret[$prop_key] = [
                ];
                break;

            }
        }
        return $ret;
    }

    protected static function defaultFieldSettingsCustomFieldBase()
    {
        $ret = [];
        $props = static::getAllProperties();
        foreach ($props as $prop_key => $prop_data) {
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
            switch ($prop_data_type) {
            case "float":
            case "integer":
                $ret[$prop_key] = [
                'min' => '',
                'max' => '',
                'prefix' => '',
                'suffix' => '',
                ];
                break;

            case "boolean":
                $ret[$prop_key] = [
                    'on_label' => new TranslatableMarkup('On'),
                    'off_label' => new TranslatableMarkup('Off'),
                ];
                break;

            case 'string':
                $ret[$prop_key] = [
                ];
                break;
            case "uri":
                $ret[$prop_key] = [
                ];
                break;

            }
        }
        return $ret;
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultStorageSettings()
    {
        return [] + static::defaultStorageSettingsCustomFieldBase()
        + parent::defaultStorageSettings();
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultFieldSettings()
    {
        return [] + static::defaultFieldSettingsCustomFieldBase()
            + parent::defaultFieldSettings();
    }

    /**
     * Returns all the metadata necessary to infer property defintion.
     *
     * @return mixed
     *   metadata about properties
     */
    abstract public static function getAllProperties();

    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition)
    {
        // Prevent early t() calls by using the TranslatableMarkup.
        $properties = [];
        foreach (static::getAllProperties() as $prop_key => $prop_data) {
            if (!array_key_exists("type", $prop_data)) {
                throw new \Exception("missing type in property definition");
            }
            $title = array_key_exists("title", $prop_data) ? $prop_data["title"] : $prop_key;
            $properties[$prop_key] = DataDefinition::create(array_key_exists("type", $prop_data) ? $prop_data["type"] : "string")
                ->setLabel(new TranslatableMarkup($title))
                ->setRequired(array_key_exists("required", $prop_data) ? $prop_data["required"] : false);
        }
        return $properties;
    }

    /**
     * {@inheritdoc}
     */
    public static function mainPropertyName()
    {
        foreach (static::getAllProperties() as $prop_key => $prop_data) {
            if (!array_key_exists("type", $prop_data)) {
                throw new \Exception("missing type in property definition");
            }
            return $prop_key;
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function schema(FieldStorageDefinitionInterface $field_definition)
    {
        $schema = ["columns" => []];

        foreach (static::getAllProperties() as $prop_key => $prop_data) {
            $prop_schema = [];
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
            switch ($prop_data_type) {
            case "integer":
                $prop_schema['type'] = 'int';
                $prop_schema['unsigned'] = array_key_exists("unsigned", $prop_data) ? $prop_data["unsigned"] : false;
                $prop_schema['size'] =
                  array_key_exists("size", $prop_data) ? $prop_data["size"] : "normal";
                break;

            case "float":
                $prop_schema['type'] = 'float';
                break;

            case "boolean":
                $prop_schema['type'] = 'int';
                $prop_schema['size'] = 'tiny';
                break;

            case "string":
                $is_ascii = array_key_exists("is_ascii", $prop_data) ? $prop_data["is_ascii"] : false;
                $case_sensitive =
                  array_key_exists("case_sensitive", $prop_data) ? (bool) $prop_data["case_sensitive"] : false;
                $prop_schema['type'] = $is_ascii ? 'varchar_ascii' : 'varchar';
                $prop_schema['length'] = array_key_exists("max_length", $prop_data) ? (int) $prop_data["max_length"] : 255;
                $prop_schema['binary'] = $case_sensitive;
                break;
            case "uri":
                $prop_schema['type'] = 'varchar';
                $prop_schema['length'] = array_key_exists("max_length", $prop_data) ? (int) $prop_data["max_length"] : 2048;
                break;

            default:
                throw new \Exception(t("unknown property type @type", ["@type" => $prop_data_type]));
            }
            $schema["columns"][$prop_key] = $prop_schema;
        }
        return $schema;
    }

    /**
     * Alters value set for a field property.
     *
     * @param string $property_name
     *   Property name.
     * @param mixed  $value
     *   Initial value of the property.
     * @param mixed  $props
     *   Metadata about properties of this field.
     *
     * @return bool
     *   return true if the value was altered, false otherwise
     */
    protected function alterValue($property_name, &$value, &$props)
    {
        if (!array_key_exists($property_name, $props)) {
            return false;
        }
        $prop_data = $props[$property_name];
        $prop_data_type =
        array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
        switch ($prop_data_type) {
        case "string":
            if (array_key_exists("max_length", $prop_data)) {
                $l = (int) $prop_data["max_length"];
                if (strlen($value) > $l) {
                    $value = substr($value, 0, $l);
                    return true;
                }
            }
            break;

        case "integer":
            if (array_key_exists("unsigned", $prop_data)) {
                $is_unsigned = (bool) $prop_data["unsigned"];
                if ($is_unsigned && $value < 0) {
                    throw new \Exception("value negative for an unsigned property in field");
                }
            }
            break;

        case "boolean":
            if ($value !== true && $value !== false) {
                $value = ($value) ? true : false;
                return true;
            }
            break;
        case "uri":

            break;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function setValue($values, $notify = true)
    {
        $props = static::getAllProperties();
        foreach (array_keys($values) as $property_name) {
            if ($this->alterValue($property_name, $values[$property_name], $props)) {
                // $values[$property_name]
            }
        }
        // TODO: Change the autogenerated stub.
        return parent::setValue($values, $notify);
        /*
        $this->values = [];
        if (!isset($values)) {
        return;
        }

        if (!is_array($values)) {
        if ($values instanceof MapItem) {
        $values = $values->getValue();
        }
        else {
        $values = unserialize($values, ['allowed_classes' => FALSE]);
        }
        }

        $this->values = $values;

        // Notify the parent of any changes.
        if ($notify && isset($this->parent)) {
        $this->parent->onChange($this->name);
        }
        */
    }

    /**
     * {@inheritdoc}
     */
    public function getConstraints()
    {
        $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
        $constraints = parent::getConstraints();

        $settings = $this->getSettings();
        $label = $this->getFieldDefinition()->getLabel();

        foreach (static::getAllProperties() as $prop_key => $prop_data) {
            $label = $this->getFieldDefinition()->getLabel() . " : " . (array_key_exists("title", $prop_data) ? $prop_data["title"] : $prop_key);
            if (array_key_exists("min", $prop_data) && $prop_data['min'] !== '') {
                $min = $prop_data['min'];
                $constraints[] = $constraint_manager->create(
                    'ComplexData', [
                    $prop_key => [
                    'Range' => [
                    'min' => $min,
                    'minMessage' => t('%name: the value may be no less than %min.', ['%name' => $label, '%min' => $min]),
                    ],
                    ],
                    ]
                );
            }

            if (array_key_exists("max", $prop_data) && $prop_data['max'] !== '') {
                $max = $prop_data['max'];
                $constraints[] = $constraint_manager->create(
                    'ComplexData', [
                    $prop_key => [
                    'Range' => [
                    'max' => $max,
                    'maxMessage' => t('%name: the value may be no greater than %max.', ['%name' => $label, '%max' => $max]),
                    ],
                    ],
                    ]
                );
            }
            // If this is an unsigned integer, add a validation constraint for the
            // integer to be positive.
            if (array_key_exists("unsigned", $prop_data) && $prop_data['unsigned']) {
                $constraints[] = $constraint_manager->create(
                    'ComplexData', [
                    $prop_key => [
                    'Range' => [
                    'min' => 0,
                    'minMessage' => t(
                        '%name: The integer must be larger or equal to %min.', [
                        '%name' => $label,
                        '%min' => 0,
                        ]
                    ),
                    ],
                    ],
                    ]
                );
            }
            if (array_key_exists("max_length", $prop_data) && $max_length = $prop_data['max_length']) {
                $constraints[] = $constraint_manager->create(
                    'ComplexData', [
                    $prop_key => [
                    'Length' => [
                    'max' => $max_length,
                    'maxMessage' => t('%name: may not be longer than @max characters.', ['%name' => $label, '@max' => $max_length]),
                    ],
                    ],
                    ]
                );
            }
        }

        return $constraints;
    }

    /**
     * {@inheritdoc}
     */
    public static function generateSampleValue(FieldDefinitionInterface $field_definition)
    {
        $values = [];
        $random = new Random();
        foreach (static::getAllProperties() as $prop_key => $prop_data) {
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
            switch ($prop_data_type) {
            case "integer":
                $min = array_key_exists("min", $prop_data) ? $prop_data["min"] : 0;
                $max = array_key_exists("max", $prop_data) ? $prop_data["max"] : 999;
                $values[$prop_key] = mt_rand($min, $max);
                break;

            case "float":
                $precision = rand(10, 32);
                $scale = rand(0, 2);
                $max = (array_key_exists("max", $prop_data) && is_numeric($prop_data['max'])) ? $prop_data['max'] : pow(10, ($precision - $scale)) - 1;
                $min = (array_key_exists("min", $prop_data) && is_numeric($prop_data['min'])) ? $prop_data['min'] : -pow(10, ($precision - $scale)) + 1;
                // @see "Example #1 Calculate a random floating-point number" in
                // http://php.net/manual/function.mt-getrandmax.php
                $random_decimal = $min + mt_rand() / mt_getrandmax() * ($max - $min);
                $values[$prop_key] = static::truncateDecimal($random_decimal, $scale);
                break;

            case "string":
                $values[$prop_key] = $random->word(mt_rand(1, array_key_exists("max_length", $prop_data) ? $prop_data["max_length"] : 255));
                break;

            case "boolean":
                $scale = rand(0, 2);
                $values[$prop_key] = ($scale < 1) ? true : false;
                break;
            case "uri":
                $random = new Random();
                if (true) {
                    // Set of possible top-level domains.
                    $tlds = ['com', 'net', 'gov', 'org', 'edu', 'biz', 'info'];
                    // Set random length for the domain name.
                    $domain_length = mt_rand(7, 15);
                    $values[$prop_key] = 'http://www.' . $random->word($domain_length) . '.' . $tlds[mt_rand(0, (count($tlds) - 1))];
                }
                else {
                    $values[$prop_key] = 'base:' . $random->name(mt_rand(1, 64));
                }
                break;
            }
        }
        return $values;
    }

    /**
     * Helper method to truncate a decimal number to a given number of decimals.
     *
     * @param float $decimal
     *   Decimal number to truncate.
     * @param int   $num
     *   Number of digits the output will have.
     *
     * @return float
     *   Decimal number truncated.
     */
    protected static function truncateDecimal($decimal, $num)
    {
        return floor($decimal * pow(10, $num)) / pow(10, $num);
    }

    /**
     * {@inheritdoc}
     */
    public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data)
    {
        $elements = [];

        $props = static::getAllProperties();
        foreach ($props as $prop_key => $prop_data) {
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";

            switch ($prop_data_type) {
            case "float":
            case "integer":
            case "boolean":
                break;

            case 'string':
                $elements[$prop_key] = [
                "#type" => "details",
                "#tree" => true,
                "#open" => true,
                "#title" => (array_key_exists("title", $prop_data) ? $prop_data["title"] : $prop_key),
                ];
                $elements[$prop_key]['max_length'] = [
                '#type' => 'number',
                '#title' => t('Maximum length'),
                '#default_value' => $this->getSetting($prop_key)['max_length'],
                '#required' => true,
                '#description' => t('The maximum length of the field in characters.'),
                '#min' => 1,
                '#disabled' => $has_data,
                ];
                break;
            }
        }

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function fieldSettingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = [];

        $props = static::getAllProperties();
        foreach ($props as $prop_key => $prop_data) {
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";

            $settings = $this->getSetting($prop_key);

            switch ($prop_data_type) {
            case "integer":
            case "float":
                $elements[$prop_key] = [
                "#type" => "details",
                "#tree" => true,
                "#open" => true,
                "#title" => (array_key_exists("title", $prop_data) ? $prop_data["title"] : $prop_key),
                ];
                $elements[$prop_key]['min'] = [
                '#type' => 'number',
                '#title' => t('Minimum'),
                '#default_value' => $settings['min'],
                '#description' => t('The minimum value that should be allowed in this field. Leave blank for no minimum.'),
                ];
                $elements[$prop_key]['max'] = [
                '#type' => 'number',
                '#title' => t('Maximum'),
                '#default_value' => $settings['max'],
                '#description' => t('The maximum value that should be allowed in this field. Leave blank for no maximum.'),
                ];
                $elements[$prop_key]['prefix'] = [
                '#type' => 'textfield',
                '#title' => t('Prefix'),
                '#default_value' => $settings['prefix'],
                '#size' => 60,
                '#description' => t("Define a string that should be prefixed to the value, like '$ ' or '&euro; '. Leave blank for none. Separate singular and plural values with a pipe ('pound|pounds')."),
                ];
                $elements[$prop_key]['suffix'] = [
                '#type' => 'textfield',
                '#title' => t('Suffix'),
                '#default_value' => $settings['suffix'],
                '#size' => 60,
                '#description' => t("Define a string that should be suffixed to the value, like ' m', ' kb/s'. Leave blank for none. Separate singular and plural values with a pipe ('pound|pounds')."),
                ];
                break;

            case 'string':
                break;

            case 'boolean':
                break;

            case 'uri':
                $elements[$prop_key] = [
                "#type" => "details",
                "#tree" => true,
                "#open" => true,
                "#title" => (array_key_exists("title", $prop_data) ? $prop_data["title"] : $prop_key),
                ];
                break;
            }
            if ($prop_data_type == "float") {
                $elements[$prop_key]['min']['#step'] = 'any';
                $elements[$prop_key]['max']['#step'] = 'any';
            }
        }

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty()
    {
        $n_total = 0;
        $n_empty = 0;
        foreach (static::getAllProperties() as $prop_key => $prop_data) {
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
            $n_total++;
            switch ($prop_data_type) {
            case "integer":
            case "float":
                $val = $this->get($prop_key)->getValue();
                if (empty($val) && (string) $val !== '0') {
                    $n_empty++;
                }
                break;

            case "boolean":
                $val = $this->get($prop_key)->getValue();
                if ($val === null) {
                    $n_empty++;
                }
                break;

            case "string":
                $value = $this->get($prop_key)->getValue();
                if ($value === null || $value === '') {
                    $n_empty++;
                }
                break;
            case "uri":
                $value = $this->get($prop_key)->getValue();
                if ($value === null || $value === '') {
                    $n_empty++;
                }
                break;
            }
        }
        return $n_empty >= $n_total;
    }

}
