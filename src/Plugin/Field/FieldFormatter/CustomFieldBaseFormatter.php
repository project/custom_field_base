<?php

namespace Drupal\custom_field_base\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class to faciliate declaration of field formatters.
 *
 * This base class is derived to faciliate declaration of field formatters
 * for field types declared wth custom_field_base module.
 */
abstract class CustomFieldBaseFormatter extends FormatterBase implements ContainerFactoryPluginInterface
{

    /**
     * The entity type manager.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * Constructs a StringFormatter instance.
     *
     * @param string                                         $plugin_id
     *   The plugin_id for the formatter.
     * @param mixed                                          $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\Core\Field\FieldDefinitionInterface    $field_definition
     *   The definition of the field to which the formatter is associated.
     * @param array                                          $settings
     *   The formatter settings.
     * @param string                                         $label
     *   The formatter label display setting.
     * @param string                                         $view_mode
     *   The view mode.
     * @param array                                          $third_party_settings
     *   Any third party settings settings.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager.
     */
    public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager)
    {
        parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

        $this->entityTypeManager = $entity_type_manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $plugin_id,
            $plugin_definition,
            $configuration['field_definition'],
            $configuration['settings'],
            $configuration['label'],
            $configuration['view_mode'],
            $configuration['third_party_settings'],
            $container->get('entity_type.manager')
        );
    }

    /**
     * Returns all the metadata necessary to infer property defintion.
     *
     * @return mixed
     *   metadata about properties
     */
    abstract public static function getAllProperties();


    protected  function getBooleanOutputFormats($prop_key)
    {
        $formats = [
          'default' => [$this->getFieldSetting($prop_key)['on_label'], $this->getFieldSetting($prop_key)['off_label']],
          'yes-no' => [t('Yes'), t('No')],
          'true-false' => [t('True'), t('False')],
          'on-off' => [t('On'), t('Off')],
          'enabled-disabled' => [t('Enabled'), t('Disabled')],
          'boolean' => [1, 0],
          'unicode-yes-no' => ['✔', '✖'],
          'custom' => t('Custom'),
        ];

        return $formats;
    }
    /**
     * Returns the default settings according to metadata about properties.
     *
     * @return mixed
     *   default settings
     */
    protected static function defaultSettingsCustomFieldBase()
    {
        $ret = [];
        $props = static::getAllProperties();
        foreach ($props as $prop_key => $prop_data) {
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
            switch ($prop_data_type) {
            case "float":
                $ret[$prop_key] = [
                'thousand_separator' => '',
                'decimal_separator' => '.',
                'scale' => 2,
                'prefix_suffix' => true,
                ];
                break;

            case "integer":
                $ret[$prop_key] = [
                'thousand_separator' => '',
                'prefix_suffix' => true,
                ];
                break;

            case 'string':
                $ret[$prop_key] = [
                'link_to_entity' => false,
                ];
                break;
            case "boolean":
                $ret[$prop_key] = [];
                $settings[$prop_key] ['format'] = 'default';
                $settings[$prop_key] ['format_custom_false'] = '';
                $settings[$prop_key] ['format_custom_true'] = '';
                break;
            }
        }
        return $ret;
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return array_merge_recursive(static::defaultSettingsCustomFieldBase(), parent::defaultSettings());
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = parent::settingsForm($form, $form_state);
        $entity_type = $this->entityTypeManager->getDefinition($this->fieldDefinition->getTargetEntityTypeId());

        $numeric_options = [
        ''  => t('- None -'),
        '.' => t('Decimal point'),
        ',' => t('Comma'),
        ' ' => t('Space'),
        chr(8201) => t('Thin space'),
        "'" => t('Apostrophe'),
        ];

        $props = static::getAllProperties();
        foreach ($props as $prop_key => $prop_data) {
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
            $elements[$prop_key] = [
            "#type" => "details",
            "#open" => true,
            "#tree" => true,
            "#title" => (array_key_exists("title", $prop_data) ? $prop_data["title"] : $prop_key),
            ];
            $settings = $this->getSetting($prop_key);
            switch ($prop_data_type) {
            case "float":
            case "integer":
                $elements[$prop_key]['thousand_separator'] = [
                '#type' => 'select',
                '#title' => t('Thousand marker'),
                '#options' => $numeric_options,
                '#default_value' => $settings['thousand_separator'],
                '#weight' => 0,
                ];

                $elements[$prop_key]['prefix_suffix'] = [
                '#type' => 'checkbox',
                '#title' => t('Display prefix and suffix'),
                '#default_value' => $settings['prefix_suffix'],
                '#weight' => 10,
                ];
                break;

            case "string":
                $elements[$prop_key]['link_to_entity'] = [
                '#type' => 'checkbox',
                '#title' => $this->t('Link to the @entity_label', ['@entity_label' => $entity_type->getLabel()]),
                '#default_value' => $settings['link_to_entity'],
                ];
                break;
            case "boolean":
                $formats = [];
                foreach ($this->getBooleanOutputFormats($prop_key) as $format_name => $format) {
                    if (is_array($format)) {
                        if ($format_name == 'default') {
                            $formats[$format_name] = $this->t('Field settings (@on_label / @off_label)', ['@on_label' => $format[0], '@off_label' => $format[1]]);
                        }
                        else {
                            $formats[$format_name] = $this->t('@on_label / @off_label', ['@on_label' => $format[0], '@off_label' => $format[1]]);
                        }
                    }
                    else {
                        $formats[$format_name] = $format;
                    }
                }
                $theSettings = $this->getSetting($prop_key);
                $default_value = (is_array($theSettings) && array_key_exists("format", $theSettings)) ? $theSettings["format"] : "boolean";
                $elements[$prop_key]['format'] = [
                '#type' => 'select',
                '#title' => $this->t('Output format'),
                '#default_value' => $default_value,
                '#options' => $formats,
                ];
                break;
            }
            if ($prop_data_type == "float") {
                $elements[$prop_key]['decimal_separator'] = [
                '#type' => 'select',
                '#title' => t('Decimal marker'),
                '#options' => ['.' => t('Decimal point'), ',' => t('Comma')],
                '#default_value' => $settings['decimal_separator'],
                '#weight' => 5,
                ];
                $elements[$prop_key]['scale'] = [
                '#type' => 'number',
                '#title' => t('Scale', [], ['context' => 'decimal places']),
                '#min' => 0,
                '#max' => 10,
                '#default_value' => $settings['scale'],
                '#description' => t('The number of digits to the right of the decimal.'),
                '#weight' => 6,
                ];
            }
        }
        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = [];
        $entity_type = $this->entityTypeManager->getDefinition($this->fieldDefinition->getTargetEntityTypeId());
        $props = static::getAllProperties();

        foreach ($props as $prop_key => $prop_data) {
            $settings = $this->getSetting($prop_key);
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
            $title = (array_key_exists("title", $prop_data) ? $prop_data["title"] : $prop_key);
            switch ($prop_data_type) {
            case "float":
            case "integer":

                break;

            case "string":
                if ($settings['link_to_entity']) {
                    $summary[] = $title . " -> " . $this->t('Linked to the @entity_label', ['@entity_label' => $entity_type->getLabel()]);
                }
                break;
            }
        }
        return $summary;
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];

        $settings = $this->getSettings();
        $field_settings = $this->getFieldSettings();
        $url = $this->getEntityUrl($items->getEntity());
        foreach ($items as $delta => $item) {
            $elements[$delta] = $this->viewValue($item, $settings, $field_settings, $url);
        }

        return $elements;
    }

    /**
     * Generate the output appropriate for one field item.
     *
     * @param \Drupal\Core\Field\FieldItemInterface $item
     *   One field item.
     * @param array|null                            $settings
     *   Formatter settings passed as argument.
     * @param array|null                            $field_settings
     *   Field settings passed as argument.
     * @param \Drupal\Core\Url|null                 $url
     *   Entity url passed as argument.
     *
     * @return string
     *   The textual output generated.
     */
    protected function viewValue(FieldItemInterface $item, $settings = null, $field_settings = null, $url = null)
    {
        if ($settings === null) {
            $settings = $this->getSettings();
        }
        if ($field_settings === null) {
            $field_settings = $this->getFieldSettings();
        }
        if ($url === null) {
            $url = static::getEntityUrl($item->getEntity());
        }
        $props = static::getAllProperties();
        $build = [];

        foreach ($props as $prop_key => $prop_data) {
            $prop_settings = $settings[$prop_key];
            $field_prop_settings = $field_settings[$prop_key];
            $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
            $title = (array_key_exists("title", $prop_data) ? $prop_data["title"] : $prop_key);
            $item_value = $item->{$prop_key};
            $this_build = [
            "#type" => "container",
            "#attributes" => [
                "class" => ["field--label-inline"],
                "data-property-title" => $title,
                "data-property" => $prop_key,
                "data-property-type" => $prop_data_type,
                ],
            ];
            $this_build["label"] = [
            "#type" => "html_tag",
            "#tag" => "label",
            "#attributes" => [
            "class" => ["field__label"]
            ], 
            "#value" => $title
            ];
            switch ($prop_data_type) {
            case "float":
            case "integer":
                $view_value = $this->viewNumericValue($item_value, $prop_settings, $field_prop_settings);
                $this_build["content"] = $view_value;
                break;

            case "boolean":
                $view_value = $this->viewBooleanValue($item_value, $prop_settings, $field_prop_settings, $prop_key);
                $this_build["content"] = $view_value;
                break;

            case "string":
                $view_value = $this->viewStringValue($item_value);
                if ($prop_settings['link_to_entity']) {
                    $build[$prop_key] = [
                    '#type' => 'link',
                    '#title' => $view_value,
                    '#url' => $url,
                    ];
                }
                else {
                    $this_build["content"] = $view_value;
                }
                break;

            }
            $build[$prop_key] = $this_build;
        }
        // The text value has no text format assigned to it, so the user input
        // should equal the output, including newlines.
        return $build;
    }

    /**
     * Generate the output appropriate for one field item of type string.
     *
     * @param string $item_value
     *   One field item value.
     *
     * @return array
     *   The textual output generated as a render array.
     */
    public function viewStringValue($item_value)
    {
        // The text value has no text format assigned to it, so the user input
        // should equal the output, including newlines.
        return [
        '#type' => 'inline_template',
        '#template' => '{{ value|nl2br }}',
        '#context' => ['value' => $item_value],
        ];
    }

    /**
     * Generate the output appropriate for one field item of type numeric.
     *
     * @param string $item_value
     *   One field item value.
     * @param mixed  $prop_settings
     *   Formatter Settings for this property.
     * @param mixed  $prop_field_settings
     *   Field Settings for this property.
     *
     * @return array
     *   The textual output generated as a render array.
     */
    public function viewNumericValue($item_value, $prop_settings, $prop_field_settings)
    {
        $output = number_format(
            $item_value,
            array_key_exists("scale", $prop_settings) ? $prop_settings["scale"] : 0,
            array_key_exists("decimal_separator", $prop_settings) ? $prop_settings["decimal_separator"] : '',
            array_key_exists("thousand_separator", $prop_settings) ? $prop_settings["thousand_separator"] : ''
        );

        // Account for prefix and suffix.
        if ($prop_settings['prefix_suffix']) {
            $prefixes = isset($prop_field_settings['prefix']) ? array_map(['Drupal\Core\Field\FieldFilteredMarkup', 'create'], explode('|', $prop_field_settings['prefix'])) : [''];
            $suffixes = isset($prop_field_settings['suffix']) ? array_map(['Drupal\Core\Field\FieldFilteredMarkup', 'create'], explode('|', $prop_field_settings['suffix'])) : [''];
            $prefix = (count($prefixes) > 1) ? $this->formatPlural($item_value, $prefixes[0], $prefixes[1]) : $prefixes[0];
            $suffix = (count($suffixes) > 1) ? $this->formatPlural($item_value, $suffixes[0], $suffixes[1]) : $suffixes[0];
            $output = $prefix . $output . $suffix;
        }
        // Output the raw value in a content attribute if the text of the HTML
        // element differs from the raw value (for example when a prefix is used).
        /* if (isset($item->_attributes) && $item->value != $output) {
        $item->_attributes += ['content' => $item->value];
        }*/

        return ['#markup' => $output];
    }

    /**
     * Generate the output appropriate for one field item of type boolean.
     *
     * @param string $item_value
     *   One field item value.
     * @param mixed  $prop_settings
     *   Formatter Settings for this property.
     * @param mixed  $prop_field_settings
     *   Field Settings for this property.
     *
     * @return array
     *   The textual output generated as a render array.
     */
    public function viewBooleanValue($item_value, $prop_settings, $prop_field_settings, $prop_key)
    {
        $formats = $this->getBooleanOutputFormats($prop_key);

        $format = array_key_exists("format", $prop_settings) ? $prop_settings['format']: "boolean";

        if ($format == 'custom') {
            return ['#markup' => $item_value ? $prop_field_settings['format_custom_true'] : $prop_field_settings['format_custom_false']];
        }
        else {
            return ['#markup' => $item_value ? $formats[$format][0] : $formats[$format][1]];
        }
    }

    /**
     * Gets the URI elements of the entity.
     *
     * @param \Drupal\Core\Entity\EntityInterface $entity
     *   The entity object.
     *
     * @return \Drupal\Core\Url
     *   The URI elements of the entity.
     */
    public function getEntityUrl(EntityInterface $entity)
    {
        // For the default revision, the 'revision' link template falls back to
        // 'canonical'.
        // @see \Drupal\Core\Entity\Entity::toUrl()
        $rel = $entity->getEntityType()->hasLinkTemplate('revision') ? 'revision' : 'canonical';
        return $entity->toUrl($rel);
    }

}
